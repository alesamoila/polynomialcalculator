package Model;

public class Monom implements Comparable<Monom> {

	protected int coeficient;
	protected int grad;

	public Monom() {

	}

	public Monom(int coeficient, int grad) {
		if (grad < 0) {
			throw new IllegalArgumentException("Puterea nu poate sa fie negativa:  " + grad);
		}
		this.coeficient = coeficient;
		this.grad = grad;
	}

	public int getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	@Override
	public String toString() {
		return coeficient + "X^" + grad;
	}

	public int compareTo(Monom Comp) {
		int CompG = Comp.getGrad();
		if (this.grad > CompG)
			return -1;
		else if (this.grad == CompG)
			return 0;
		else
			return 1;
	}

}

