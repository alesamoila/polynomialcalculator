package Model;

//import java.awt.List;
import java.util.*;

public class Operatii {

	public Operatii() {

	}

	/*
	 * public Polinom adunare(Polinom p1, Polinom p2) { List<Monom> rezultat = new
	 * ArrayList<Monom>(); Polinom copie1 = new Polinom(); Polinom copie2 = new
	 * Polinom(); Polinom rezF = new Polinom();
	 * 
	 * Monom max1 = new Monom(); Monom max2 = new Monom();
	 * 
	 * copie1 = p1.creazaCopie(p1); copie2 = p2.creazaCopie(p2); Monom var = new
	 * Monom();
	 * 
	 * max1 = copie1.GradMaxMonom(); max2 = copie2.GradMaxMonom(); while
	 * ((!copie1.getPolinom().isEmpty()) && (!copie2.getPolinom().isEmpty())) { if
	 * (max1.getGrad() == max2.getGrad()) { var.setCoeficient(max1.getCoeficient() +
	 * max2.getCoeficient()); var.setGrad(max1.getGrad()); rezultat.add(var);
	 * copie1.getPolinom().remove(max1); copie2.getPolinom().remove(max2); max1 =
	 * copie1.GradMaxMonom(); max2 = copie2.GradMaxMonom(); } else if
	 * (max1.getGrad() > max2.getGrad()) { rezultat.add(max1);
	 * copie1.getPolinom().remove(max1); max1 = copie1.GradMaxMonom(); } else {
	 * rezultat.add(max2); copie2.getPolinom().remove(max2); max2 =
	 * copie2.GradMaxMonom(); } } if (!copie1.getPolinom().isEmpty())
	 * copie1.copiaza(copie1, rezultat); if (!copie2.getPolinom().isEmpty())
	 * copie2.copiaza(copie2, rezultat); // rezultat.verificaZero(rezultat);
	 * rezF.setPolinom(rezultat); return rezF; }
	 */

	/*
	 * public Polinom scadere(Polinom p1, Polinom p2) { Polinom rezultat = new
	 * Polinom(); Polinom copie1 = new Polinom(); Polinom copie2 = new Polinom();
	 * 
	 * Monom max1 = new Monom(); Monom max2 = new Monom();
	 * 
	 * copie1 = p1.creazaCopie(p1); copie2 = p2.creazaCopie(p2); Monom var = new
	 * Monom();
	 * 
	 * max1 = copie1.GradMaxMonom(); max2 = copie2.GradMaxMonom(); while
	 * ((!copie1.getPolinom().isEmpty()) && (!copie2.getPolinom().isEmpty())) { if
	 * (max1.getGrad() == max2.getGrad()) { var.setCoeficient(max1.getCoeficient() -
	 * max2.getCoeficient()); var.setGrad(max1.getGrad());
	 * rezultat.getPolinom().add(var); } else if (max1.getGrad() > max2.getGrad()) {
	 * rezultat.getPolinom().add(max1); copie1.getPolinom().remove(max1); max1 =
	 * copie1.GradMaxMonom(); } else { rezultat.getPolinom().add(max2);
	 * copie2.getPolinom().remove(max2); max2 = copie2.GradMaxMonom(); } } if
	 * (!copie1.getPolinom().isEmpty()) copie1.copiaza(copie1, rezultat); if
	 * (!copie2.getPolinom().isEmpty()) copie2.copiaza(copie2, rezultat);
	 * rezultat.verificaZero(rezultat); return rezultat; }
	 */

	public Polinom adunare(Polinom p1, Polinom p2) {
		List<Monom> rezultatP = new ArrayList<Monom>();
		Polinom rezultatF = new Polinom();

		for (Monom monom1 : p1.getPolinom()) {
			boolean ok = true;
			for (Monom monom2 : p2.getPolinom()) {
				if (monom1.getGrad() == monom2.getGrad()) {
					Monom monomAux = new Monom();
					monomAux.setCoeficient(monom1.getCoeficient() + monom2.getCoeficient());
					monomAux.setGrad(monom1.getGrad());
					rezultatP.add(monomAux);
					ok = false;
				}
			}
			if (ok == true) {
				rezultatP.add(monom1);
			}
		}

		for (Monom monom2 : p2.getPolinom()) {
			boolean ok = true;
			for (Monom monom1 : p1.getPolinom()) {
				if (monom1.getGrad() == monom2.getGrad()) {
					ok = false;
				}
			}
			if (ok == true) {
				rezultatP.add(monom2);
			}
		}
		rezultatP.sort(null);
		rezultatF.setPolinom(rezultatP);
		return rezultatF;
	}

	public Polinom scadere(Polinom p1, Polinom p2) {
		List<Monom> rezultatP = new ArrayList<Monom>();
		Polinom rezultatF = new Polinom();

		for (Monom monom1 : p1.getPolinom()) {
			boolean ok = true;
			for (Monom monom2 : p2.getPolinom()) {
				if (monom1.getGrad() == monom2.getGrad()) {
					Monom monomAux = new Monom();
					monomAux.setCoeficient(monom1.getCoeficient() - monom2.getCoeficient());
					monomAux.setGrad(monom1.getGrad());
					rezultatP.add(monomAux);
					ok = false;
				}
			}
			if (ok == true) {
				rezultatP.add(monom1);
			}
		}

		for (Monom monom2 : p2.getPolinom()) {
			boolean ok = true;
			for (Monom monom1 : p1.getPolinom()) {
				if (monom1.getGrad() == monom2.getGrad()) {
					ok = false;
				}
			}
			if (ok == true) {

				monom2.setCoeficient(-monom2.getCoeficient());
				rezultatP.add(monom2);
			}
		}
		rezultatP.sort(null);
		rezultatF.setPolinom(rezultatP);
		return rezultatF;
	}

	public Polinom derivare(Polinom p1) {
		List<Monom> rezultatP = new ArrayList<Monom>();
		Polinom rezultatF = new Polinom();

		for (Monom monom : p1.getPolinom()) {
			if (monom.getGrad() > 0) {
				Monom monomAux = new Monom();
				monomAux.setCoeficient(monom.getCoeficient() * monom.getGrad());
				monomAux.setGrad(monom.getGrad() - 1);
				rezultatP.add(monomAux);
			}
		}
		rezultatP.sort(null);
		rezultatF.setPolinom(rezultatP);
		return rezultatF;
	}

	public Polinom integrare(Polinom p1) {
		List<Monom> rezultatP = new ArrayList<Monom>();
		Polinom rezultatF = new Polinom();

		for (Monom monom : p1.getPolinom()) {
			Monom monomAux = new Monom();
			if (monom.getGrad() > 0) {
				monomAux.setCoeficient(monom.getCoeficient() / (monom.getGrad() + 1));
				monomAux.setGrad(monom.getGrad() + 1);
			} else {
				monomAux.setCoeficient(monom.getCoeficient());
				monomAux.setGrad(1);
			}
			rezultatP.add(monomAux);
		}

		rezultatP.sort(null);
		rezultatF.setPolinom(rezultatP);
		return rezultatF;
	}

}
