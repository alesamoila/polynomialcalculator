package Model;

import java.util.*;
import java.util.regex.Pattern;

public class Polinom implements Iterable<Monom> {

	List<Monom> polinom = new ArrayList<Monom>();
	private String polinomS;

	public String getPolinomS() {
		return polinomS;
	}

	public void setPolinomS(String polinomS) {
		this.polinomS = polinomS;
	}

	public Polinom(ArrayList<Monom> polinom, String polinomS) {
		super();
		this.polinom = polinom;
		this.polinomS = polinomS;
	}

	public Polinom(String polinom) {
		this.polinomS = polinom;
		this.polinom = new ArrayList<Monom>();
	}

	public Polinom() {

	}

	public List<Monom> getPolinom() {
		return polinom;
	}

	public void setPolinom(List<Monom> rezultat) {
		this.polinom = rezultat;
	}

	/**
	 * 
	 * @return monomul care are gradul maxim din polinom
	 */
	public Monom GradMaxMonom() {
		Monom gMax = new Monom();

		gMax.setGrad(-1);
		for (int i = 0; i < this.polinom.size(); i++) {
			if (this.polinom.get(i).getGrad() > gMax.getGrad()) {
				gMax.setCoeficient(this.polinom.get(i).getCoeficient());
				gMax.setGrad(this.polinom.get(i).getGrad());
			}
		}
		return gMax;
	}

	public Polinom creazaCopie(Polinom sursa) {
		Polinom destinatie = new Polinom();
		for (int i = 0; i < sursa.getPolinom().size(); i++) {
			destinatie.getPolinom().add(sursa.getPolinom().get(i));
		}
		return destinatie;

	}

	public void copiaza(Polinom sursa, List<Monom> rezultat) {
		for (int i = 0; i < sursa.getPolinom().size(); i++) {
			rezultat.add(sursa.getPolinom().get(i));
		}
	}

	public Polinom verificaZero(Polinom p) {
		for (int i = 0; i < p.getPolinom().size(); i++)
			if (p.getPolinom().get(i).getCoeficient() == 0)
				p.getPolinom().remove(p.getPolinom().get(i));
		return p;
	}

	public void afis(Polinom p) {
		for (int i = 0; i < p.getPolinom().size(); i++) {
			if (p.getPolinom().get(i).getCoeficient() > 0)
				System.out.print("+");
			if (p.getPolinom().get(i).getGrad() > 1)
				System.out.print(p.getPolinom().get(i).getCoeficient() + "x^" + p.getPolinom().get(i).getGrad() + " ");
			else if (p.getPolinom().get(i).getGrad() == 1)
				System.out.print(p.getPolinom().get(i).getCoeficient() + "x ");
			else if (p.getPolinom().get(i).getGrad() == 0)
				System.out.print(p.getPolinom().get(i).getCoeficient());
		}

	}

	public List<Monom> transformare() {

		polinomS = polinomS.replaceAll("X", "x");
		polinomS = polinomS.replaceAll(" ", "");
		// inlocuim X cu x si eliminam spatiile
		polinomS = polinomS.replaceAll("-", "+-");

		String[] bucati = polinomS.split(Pattern.quote("+")); // "spargem" stringul in bucati

		for (int i = 0; i < bucati.length; i++) {

			if (!bucati[i].contains("^")) {

				if (!bucati[i].contains("x")) {
					String[] s = bucati[i].split(Pattern.quote("[x]{0,1}"));
					String coeficient = s[0];
					String grad = "0";
					polinom.add(new Monom(Integer.parseInt(coeficient), Integer.parseInt(grad))); // cand x e la puterea
																									// 0; x nu apare
				}

				else if (bucati[i].contains("x")) {
					String[] s = bucati[i].split("x");
					String coeficient = s[0];
					String grad = "1";
					polinom.add(new Monom(Integer.parseInt(coeficient), Integer.parseInt(grad))); // apare x dar are
																									// puterea 1
				}
			}

			else {
				String[] s = bucati[i].split("x" + Pattern.quote("^"));
				String coeficient = s[0];
				String grad = s[1];
				polinom.add(new Monom(Integer.parseInt(coeficient), Integer.parseInt(grad))); // x are puterea >1
			}
		}

		return polinom;
	}

	public Iterator<Monom> iterator() {
		return polinom.iterator();
	}

}
