package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.Monom;
import Model.Operatii;
import Model.Polinom;
import View.Interfata;

public class Controller {

	private Polinom pol1;
	private Polinom pol2;
	private Interfata view;

	public Controller() {
		this.view = new Interfata();
		this.view.setVisible(true);
		this.view.addButton1Listener(new Button1Listener());
		this.view.addButton2Listener(new Button2Listener());
		this.view.addButton3Listener(new Button3Listener());
		this.view.addButton4Listener(new Button4Listener());

	}

	// adunare
	public class Button1Listener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {

			// ADUNARE
			pol1 = new Polinom(view.T1.getText());
			pol2 = new Polinom(view.T2.getText());
			Polinom rezultat = new Polinom();
			String rezultatString = "";

			pol1.transformare();
			pol2.transformare();
			// transformam cele doua stringuri pentru a putea face adunarea

			Operatii op = new Operatii();

			rezultat = op.adunare(pol1, pol2);
			rezultat.verificaZero(rezultat);

			for (Monom m : rezultat) {
				if (m.getCoeficient() >= 0) {
					rezultatString = rezultatString + "+";
				}

				if (m.getGrad() > 1) {
					rezultatString = rezultatString + m.toString();
				}
				if (m.getGrad() == 1) {

					rezultatString = rezultatString + m.getCoeficient() + "x";
				}

				if (m.getGrad() == 0) {
					rezultatString = rezultatString + m.getCoeficient();
				}

				view.T3.setText(rezultatString);
			}
		}
	}

	// scadere
	private class Button2Listener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
		
			pol1 = new Polinom(view.T1.getText());
			pol2 = new Polinom(view.T2.getText());
			Polinom rezultat = new Polinom();
			String rezultatString = "";
			Operatii op = new Operatii();

			pol1.transformare();
			pol2.transformare();

			rezultat = op.scadere(pol1, pol2);
			rezultat.verificaZero(rezultat);
			for (Monom m : rezultat) {
				if (m.getCoeficient() >= 0) {
					rezultatString = rezultatString + "+";
				}
				if (m.getGrad() > 1) {
					rezultatString = rezultatString + m.toString();
				}
				if (m.getGrad() == 1) {

					rezultatString = rezultatString + m.getCoeficient() + "x";
				}

				if (m.getGrad() == 0) {
					rezultatString = rezultatString + m.getCoeficient();
				}
				view.T3.setText(rezultatString);
			}
		}
	}

	// integrare
	public class Button3Listener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
		
			pol1 = new Polinom(view.T1.getText());
			Polinom rezultat = new Polinom();
			String rezultatString = "";
			Operatii op = new Operatii();

			pol1.transformare();

			rezultat = op.integrare(pol1);
			rezultat.verificaZero(rezultat);
			for (Monom m : rezultat) {
				if (m.getCoeficient() >= 0) {
					rezultatString = rezultatString + "+";
				}
				if (m.getGrad() > 1) {
					rezultatString = rezultatString + m.toString();
				}
				if (m.getGrad() == 1) {

					rezultatString = rezultatString + m.getCoeficient() + "x";
				}

				if (m.getGrad() == 0) {
					rezultatString = rezultatString + m.getCoeficient();
				}
				view.T3.setText(rezultatString);
			}

		}

	}

	// derivare
	public class Button4Listener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
			pol1 = new Polinom(view.T1.getText());
			Polinom rezultat = new Polinom();
			String rezultatString = "";
			Operatii op = new Operatii();

			pol1.transformare();

			rezultat = op.derivare(pol1);
			// rezultat.verificaZero(rezultat);
			for (Monom m : rezultat) {
				if (m.getCoeficient() >= 0) {
					rezultatString = rezultatString + "+";
				}
				if (m.getGrad() > 1) {
					rezultatString = rezultatString + m.toString();
				}
				if (m.getGrad() == 1) {

					rezultatString = rezultatString + m.getCoeficient() + "x";
				}

				if (m.getGrad() == 0) {
					rezultatString = rezultatString + m.getCoeficient();
				}
				view.T3.setText(rezultatString);
			}

		}

	}
}

