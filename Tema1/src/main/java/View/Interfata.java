package View;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

//import proiectTP.Controller.Button0Listener;

public class Interfata extends JFrame {

	private static final long serialVersionUID = 1L;
	JFrame frame;
	public JTextField T1;
	public JTextField T2;
	public JTextField T3;

	JButton adunare;
	JButton scadere;
	JButton derivare;
	JButton integrare;
	JLabel pol1;
	JLabel pol2;
	JLabel rezultat;
	JPanel panel;

	public Interfata() {

		this.setTitle("Operatii cu polinoame");

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.setSize(new Dimension(600, 500));

		panel = new JPanel();
		this.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		T1 = new JTextField();
		T1.setBackground(Color.lightGray);
		T1.setBounds(100, 38, 150, 25);
		panel.add(T1);
		T1.setColumns(20);

		T2 = new JTextField();
		T2.setBackground(Color.lightGray);
		T2.setBounds(100, 105, 150, 25);
		panel.add(T2);
		T2.setColumns(20);

		T3 = new JTextField();
		T3.setBounds(82, 172, 198, 25);
		T3.setBackground(Color.white);
		T3.setEditable(false);
		panel.add(T3);
		T3.setColumns(20);

		pol1 = new JLabel("Polinom 1");
		pol1.setBounds(26, 41, 60, 14);
		panel.add(pol1);

		pol2 = new JLabel("Polinom 2");
		pol2.setBounds(26, 108, 60, 14);
		panel.add(pol2);

		rezultat = new JLabel("Rezultat");
		rezultat.setBounds(26, 175, 46, 14);
		panel.add(rezultat);

		adunare = new JButton("Adunare");
		adunare.setBounds(278, 35, 89, 23);
		panel.add(adunare);

		scadere = new JButton("Scadere");
		scadere.setBounds(278, 59, 89, 23);
		panel.add(scadere);

		integrare = new JButton("Integrare");
		integrare.setBounds(278, 82, 89, 23);
		panel.add(integrare);

		derivare = new JButton("Derivare");
		derivare.setBounds(278, 104, 89, 23);
		panel.add(derivare);

		this.setContentPane(panel);
		this.revalidate();
		this.repaint();

	}

	public void addButton1Listener(ActionListener l) {
		adunare.addActionListener(l);
	}

	public void addButton2Listener(ActionListener l) {
		scadere.addActionListener(l);
	}

	public void addButton3Listener(ActionListener l) {
		integrare.addActionListener(l);
	}

	public void addButton4Listener(ActionListener l) {
		derivare.addActionListener(l);
	}

}
